<?php

namespace Develdesign\ZendeskWrapper;

use Illuminate\Support\ServiceProvider;

class ZendeskServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/zendesk.php' => config_path('zendesk.php'),
        ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerZendesk();

        $this->mergeConfig();
    }

    /**
     * Register the application bindings.
     *
     * @return void
     */
    private function registerZendesk()
    {
        $this->app->bind('zendesk', function ($app) {
            return new Zendesk($app);
        });
        
        $this->app->alias('zendesk', 'Develdesign\ZendeskWrapper');
    }

    /**
     * Merges user's and entrust's configs.
     *
     * @return void
     */
    private function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/zendesk.php', 'zendesk'
        );
    }
}
