<?php
 
return [
    'credentials' => [
        'subdomain' => 'http://<subdomain>.zendesk.com',
        'username' => '<username>',
        'token' => '<token>'
    ]
];