<?php 

namespace Develdesign\ZendeskWrapper;

use InvalidArgumentException, BadMethodCallException;
use Zendesk\API\HttpClient;

class Zendesk {
 
    /**
     * Zendesk subdomain.
     *
     * @var string
     */
    protected $subdomain;

    /**
     * Zendesk username.
     *
     * @var string
     */
    protected $username;

    /**
     * Zendesk token.
     *
     * @var string
     */
    protected $token;

    /**
     * Http client.
     *
     * @var HttpClient
     */
    protected $client;

    /**
     * Get auth parameters from config, fail if any are missing.
     * Instantiate API client and set auth token.
     *
     * @throws Exception
     */
    public function __construct() {
        $this->subdomain = config('zendesk.credentials.subdomain');
        $this->username = config('zendesk.credentials.username');
        $this->token = config('zendesk.credentials.token');
        if(!$this->subdomain || !$this->username || !$this->token) {
            throw new InvalidArgumentException('Please set ZENDESK_SUBDOMAIN, ZENDESK_USERNAME and ZENDESK_TOKEN environment variables.');
        }
        $this->client = new HttpClient($this->subdomain, $this->username);
        $this->client->setAuth('basic', ['username' => $this->username, 'token' => $this->token]);
    }
    /**
     * Pass any method calls onto $this->client
     *
     * @return mixed
     */
    public function __call($method, $args) {
        if(is_callable([$this->client,$method])) {
            return call_user_func_array([$this->client,$method],$args);
        } else {
            throw new BadMethodCallException("Method $method does not exist");
        }
    }
}